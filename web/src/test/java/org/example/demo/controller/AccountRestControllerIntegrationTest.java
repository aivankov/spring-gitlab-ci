package org.example.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.demo.model.Account;
import org.example.demo.repository.jpa.AccountRepository;
import org.example.demo.util.AccountDataGenerator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@SpringBootTest
@AutoConfigureMockMvc
@Sql(scripts = "/db/migration_dev/V1.0.2__create_table_h2.sql")
class AccountRestControllerIntegrationTest {

    @Value("${generate.count:0}")
    private int countGenerateData;

    private final String PART_OF_URL = "/api/accounts";
    private final long startGlobalSeq = 10;
    private final AccountDataGenerator dataGenerator;
    private final AccountRepository accountRepository;
    private final ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @Autowired
    AccountRestControllerIntegrationTest(AccountDataGenerator dataGenerator,
                                         AccountRepository accountRepository, ObjectMapper objectMapper) {
        this.dataGenerator = dataGenerator;
        this.accountRepository = accountRepository;
        this.objectMapper = objectMapper;
    }

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        accountRepository.saveAll(dataGenerator.generateAccountList(countGenerateData));
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(restDocumentation))
                .alwaysDo(document("{class-name}/{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())))
                .build();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void searchAccountById() throws Exception {
        String content = mockMvc.perform(get(PART_OF_URL)
                .param("id", String.valueOf(startGlobalSeq))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();
        Account account = objectMapper.readValue(content, Account.class);
        assertEquals(startGlobalSeq, account.getId());
    }

    @Test
    void searchAccountByEmail() throws Exception {
        String content = mockMvc.perform(get(PART_OF_URL + "/" + startGlobalSeq)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();
        Account account = objectMapper.readValue(content, Account.class);
        assertEquals(startGlobalSeq, account.getId());

        String contentByEmail = mockMvc.perform(get(PART_OF_URL)
                .param("email", account.getEmail())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();
        Account accountByEmail = objectMapper.readValue(contentByEmail, Account.class);
        assertEquals(startGlobalSeq, account.getId());
        assertEquals(account, accountByEmail);
    }

    @Test
    void getOne() throws Exception {
        String content = mockMvc.perform(get(PART_OF_URL + "/" + startGlobalSeq)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();
        Account account = objectMapper.readValue(content, Account.class);
        assertEquals(startGlobalSeq, account.getId());
    }

    @Test
    void getAll() throws Exception {
        int pageableDefault = 10;
        String content = mockMvc.perform(get(PART_OF_URL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn().getResponse().getContentAsString();
        Account[] accounts = objectMapper.readValue(content, Account[].class);
        assertEquals(pageableDefault, accounts.length);

    }

    @Test
    void save() throws Exception {
        Account generateAccount = dataGenerator.generateAccount();
        String content = mockMvc.perform(post(PART_OF_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(generateAccount)))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();
        Account account = objectMapper.readValue(content, Account.class);
        generateAccount.setId(account.getId());
        assertEquals(account, generateAccount);
    }

    @Test
    void remove() throws Exception {
        mockMvc.perform(delete(PART_OF_URL + "/" + startGlobalSeq)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void update() throws Exception {
        Account generateAccount = dataGenerator.generateAccount();
        generateAccount.setId(startGlobalSeq);
        mockMvc.perform(put(PART_OF_URL + "/" + startGlobalSeq)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(generateAccount)))
                .andExpect(status().isNoContent());
    }

    @Test
    void debitRecord() throws Exception {
        mockMvc.perform(put(PART_OF_URL + "/" + startGlobalSeq + "/debit")
                .param("value", "100")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk());
    }

    @Test
    void creditRecord() throws Exception {
        mockMvc.perform(put(PART_OF_URL + "/" + startGlobalSeq + "/credit")
                .param("value", "100")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk());
    }
}
