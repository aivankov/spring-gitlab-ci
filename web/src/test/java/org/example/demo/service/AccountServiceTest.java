package org.example.demo.service;

import org.example.demo.exception.IllegalRequestDataException;
import org.example.demo.exception.NotFoundException;
import org.example.demo.model.Account;
import org.example.demo.repository.jpa.AccountRepository;
import org.example.demo.to.AccountTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * - @SpringBootTest get up all context
 */

@ExtendWith(SpringExtension.class)
class AccountServiceTest {

    @Mock
    private AccountRepository repository;

    @InjectMocks
    private AccountService service;

    private final String EMAIL = "test@test.com";
    private final String EMAIL_NOT_FOUND = "not.found@test.com";
    private final Long ID = 1L;
    private final Long ID_NOT_FOUND = 10L;

    private final Account account = new Account();

    {
        account.setId(ID);
        account.setEmail(EMAIL);
        account.setBalance(BigDecimal.TEN);
    }

    private final AccountTo accountTo = new AccountTo();

    {
        accountTo.setId(ID);
        accountTo.setEmail(EMAIL);
        accountTo.setBalance(BigDecimal.TEN);
    }


    @BeforeEach
    void setUp() {
        when(repository.findById(ID)).thenReturn(Optional.of(account));
        when(repository.findById(ID_NOT_FOUND)).thenReturn(Optional.empty());
        when(repository.findAll(Pageable.unpaged())).thenReturn(new PageImpl<>(List.of(account)));
        when(repository.findByEmail(EMAIL)).thenReturn(Optional.of(account));
        when(repository.findByEmail(EMAIL_NOT_FOUND)).thenReturn(Optional.empty());
        when(repository.delete(ID)).thenReturn(1);
        when(repository.delete(ID_NOT_FOUND)).thenReturn(0);
    }

    @Test
    void get() {
        Account account = service.get(ID);
        assertTrue(Objects.nonNull(account));
        verify(repository, times(1)).findById(ID);
    }

    @Test
    void getWrongId() {
        assertThrows(NotFoundException.class, () -> service.get(ID_NOT_FOUND));
        verify(repository, times(1)).findById(ID_NOT_FOUND);
    }

    @Test
    void getAll() {
        Page<Account> accountsPage = service.getAll(Pageable.unpaged());
        assertFalse(accountsPage.getContent().isEmpty());
        verify(repository, times(1)).findAll(Pageable.unpaged());
    }

    @Test
    void getByEmail() {
        Account account = service.getByEmail(EMAIL);
        assertTrue(Objects.nonNull(account));
        verify(repository, times(1)).findByEmail(EMAIL);
    }

    @Test
    void getByEmailWrong() {
        assertThrows(NotFoundException.class, () -> service.getByEmail(EMAIL_NOT_FOUND));
        verify(repository, times(1)).findByEmail(EMAIL_NOT_FOUND);
    }

    @Test
    void delete() {
        assertDoesNotThrow(() -> service.delete(ID));
        verify(repository, times(1)).delete(ID);
    }

    @Test
    void deleteWrongId() {
        assertThrows(NotFoundException.class, () -> service.delete(ID_NOT_FOUND));
        verify(repository, times(1)).delete(ID_NOT_FOUND);
    }

    @Test
    void update() {
        assertDoesNotThrow(() -> service.update(accountTo, ID));
        verify(repository, times(1)).save(account);
    }

    @Test
    void updateWrongId() {
        AccountTo accountToWrong = new AccountTo();
        accountToWrong.setId(ID_NOT_FOUND);
        assertThrows(IllegalRequestDataException.class, () -> service.update(accountToWrong, ID));
        verify(repository, times(0)).save(account);
    }

    @Test
    void updateEmptyId() {
        AccountTo accountTo = new AccountTo();
        assertThrows(IllegalRequestDataException.class, () -> service.update(accountTo, ID));
        verify(repository, times(0)).save(account);
    }

    @Test
    void save() {
        AccountTo accountToNew = new AccountTo();
        assertDoesNotThrow(() -> service.save(accountToNew));
        verify(repository, times(1)).save(new Account());
    }

    @Test

    void saveWithNotEmptyId() {
        assertThrows(IllegalRequestDataException.class, () -> service.save(accountTo));
        verify(repository, times(0)).save(account);
    }

    @Test
    void debitRecord() {
        BigDecimal bigDecimal = service.debitRecord(ID, BigDecimal.ONE);
        assertEquals(0, bigDecimal.compareTo(BigDecimal.TEN.add(BigDecimal.ONE)));
        verify(repository, times(1)).findById(ID);
    }

    @Test
    void creditRecord() {
        BigDecimal bigDecimal = service.creditRecord(ID, BigDecimal.ONE);
        assertEquals(0, bigDecimal.compareTo(BigDecimal.TEN.subtract(BigDecimal.ONE)));
        verify(repository, times(1)).findById(ID);
    }
}
