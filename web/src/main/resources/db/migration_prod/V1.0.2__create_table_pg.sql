drop table if exists accounts CASCADE ;
drop sequence if exists global_seq ;
create sequence global_seq start with 10 increment by 1;
create table accounts (
    id bigint not null,
    balance numeric(19,2),
    birthday date,
    email varchar(255),
    first_name varchar(255),
    gender varchar(255),
    last_name varchar(255),
    primary key (id));
alter table accounts add constraint UK_email unique (email)
