let form = $("#detailsForm")
let failedNote

let table = $("#accounts").DataTable({
    processing: true,
    serverSide: true,
    ajax: {
        url: "/api/accounts/tables",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        rowId: "id",
        data: function (d) {
            return JSON.stringify(d)
        }
    },
    columns: [
        {data: "id", "visible": false},
        {data: "firstName", "width": "15%"},
        {data: "lastName", "width": "15%"},
        {data: "email", "width": "20%"},
        {data: "birthday", "width": "15%"},
        {data: "gender", "width": "15%"},
        {data: "balance", "width": "20%"}
    ]
})

$("#accounts tbody").on("click", "tr", function () {
    if ($(this).hasClass("selected")) {
        $(this).removeClass("selected")
    } else {
        table.$("tr.selected").removeClass("selected")
        $(this).addClass("selected")
    }
})

$("#delete").click(function () {
    if (table.row(".selected").data()) {
        let id = table.row(".selected").data().id

        $.ajax({
            url: "/api/accounts/" + id,
            type: "DELETE"
        }).done(function () {
            table.row(".selected").remove().draw("page")
            successNoty("deleted")
        })
    }
})

$("#update").click(function () {
    if (table.row(".selected").data()) {
        let id = table.row(".selected").data().id
        $.ajax({
            url: "/api/accounts/" + id,
            type: "GET"
        }).done(function (data) {
            $.each(data, function (key, value) {
                if (key === "gender") {
                    let $radios = form.find("input:radio[name=gender]")
                    $radios.filter("[value='" + value + "']").prop("checked", true)
                } else {
                    form.find("input[name='" + key + "']").val(value)
                }
            })
            $("#editRow").modal()
        })
    }
})

$("#create").click(function () {
    $("#editRow").modal()
})

$("#save").click(function () {
    form[0].classList.add("was-validated")

    if (form[0].checkValidity()) {
        form[0].classList.remove("was-validated")
        let array = form.serializeArray()
        let obj = {}
        $.map(array, function (n) {
            obj[n["name"]] = n["value"]
        })

        if (obj.id === "") {
            postAjax(obj)
        } else {
            putAjax(obj)
        }
    }
})

function postAjax(obj) {
    $.ajax({
        url: "/api/accounts/",
        type: "POST",
        data: JSON.stringify(obj),
        dataType: "json",
        contentType: "application/json",
    }).done(function () {
        $("#editRow").modal("hide")
        resetForm()

        table.draw("page")
        successNoty("saved")
    }).fail(function (event) {
        failNoty(event)
    })
}

function putAjax(obj) {
    $.ajax({
        url: "/api/accounts/" + obj.id,
        type: "PUT",
        data: JSON.stringify(obj),
        dataType: "json",
        contentType: "application/json",
    }).done(function () {
        $("#editRow").modal("hide")
        resetForm()

        table.row(".selected").remove().draw("page")
        successNoty("updated")
    }).fail(function (event) {
        failNoty(event)
    })
}

function resetForm() {
    form[0].reset()
    $("input[type=hidden]", form[0]).removeAttr("value")
}

$("#closeForm").click(function () {
    resetForm()
    closeNoty()
})

function closeNoty() {
    if (failedNote) {
        failedNote.close()
        failedNote = undefined
    }
}

function successNoty(key) {
    closeNoty()
    new Noty({
        text: "<span class='fa fa-lg fa-check'></span> &nbsp;" + key,
        type: "success",
        layout: "bottomRight",
        timeout: 1000
    }).show()
}

function failNoty(value) {
    closeNoty()
    let errorInfo = value["responseJSON"]
    failedNote = new Noty({
        text: "<span class='fa fa-lg fa-exclamation-circle'></span> &nbsp;" + errorInfo["errorType"] + "<br>" + errorInfo["errorMsg"] + "<br>",
        type: "error",
        layout: "bottomRight"
    }).show()
}
