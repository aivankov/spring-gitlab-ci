package org.example.demo.exception;

import org.springframework.http.HttpStatus;

public enum ErrorType {
    APP_ERROR("Application error", HttpStatus.INTERNAL_SERVER_ERROR),
    //  http://stackoverflow.com/a/22358422/548473
    DATA_NOT_FOUND("Data not found", HttpStatus.NOT_FOUND),
    NOT_FOUND_URL("Not found url", HttpStatus.NOT_FOUND),
    DATA_ERROR("Data error", HttpStatus.UNPROCESSABLE_ENTITY),
    VALIDATION_ERROR("Validation error", HttpStatus.UNPROCESSABLE_ENTITY),
    UNIQUE_ERROR("Unique error", HttpStatus.CONFLICT),
    BAD_REQUEST("Bad request", HttpStatus.BAD_REQUEST);

    private final String errorCode;
    private final HttpStatus status;

    ErrorType(String errorCode, HttpStatus status) {
        this.errorCode = errorCode;
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
