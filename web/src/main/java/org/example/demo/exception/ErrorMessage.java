package org.example.demo.exception;

public enum ErrorMessage {
    NOT_FOUND_ID("Account not found with id = "),
    NOT_FOUND_EMAIL("Account not found with email = ");

    private final String message;

    ErrorMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
