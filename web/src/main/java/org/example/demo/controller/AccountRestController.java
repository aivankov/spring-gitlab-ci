package org.example.demo.controller;

import org.example.demo.model.Account;
import org.example.demo.service.AccountService;
import org.example.demo.to.AccountTo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/accounts")
public class AccountRestController {

    private static final Logger log = LoggerFactory.getLogger(AccountRestController.class);
    private final AccountService accountService;

    public AccountRestController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping(params = "id")
    public ResponseEntity<Object> searchUserById(@RequestParam Long id) {
        log.debug("get account by id = {}", id);
        Optional<Account> optionalAccount = accountService.getWithOptional(id);
        return optionalAccount.map(account -> new ResponseEntity<Object>(account, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>("{}", HttpStatus.OK));
    }

    @GetMapping(params = "email")
    public Account searchUserByEmail(@RequestParam String email) {
        String emailLog = email.replaceAll("[\n\r\t]", "_");
        log.debug("get account by email = {}", emailLog);
        return accountService.getByEmail(email);
    }

    @GetMapping("/{id}")
    public Account getOne(@PathVariable("id") Long id) {
        log.debug("get account by id = {}", id);
        return accountService.get(id);
    }

    @GetMapping
    public List<Account> getAll(@PageableDefault Pageable pageable) {
        log.debug("get page = {}  with count = {} of accounts", pageable.getPageNumber(), pageable.getPageSize());
        return accountService.getAll(pageable).getContent();
    }

    @PostMapping("/tables")
    public DataTablesOutput<Account> getAll(@RequestBody DataTablesInput input) {
        log.debug("get accounts for table");
        return accountService.findAll(input);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public Account save(@Valid @RequestBody AccountTo accountTo) {
        log.debug("save account = {}", accountTo);
        return accountService.save(accountTo);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Long id) {
        log.debug("remove account by id = {}", id);
        accountService.delete(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@Valid @RequestBody AccountTo accountTo, @PathVariable Long id) {
        log.debug("update account = {}", accountTo);
        accountService.update(accountTo, id);
    }

    @PutMapping("/{id}/debit")
    public BigDecimal debitRecord(@PathVariable Long id, @RequestParam BigDecimal value) {
        log.debug("debit balance = {} for user id = {}", value, id);
        return accountService.debitRecord(id, value);
    }

    @PutMapping("/{id}/credit")
    public BigDecimal creditRecord(@PathVariable Long id, @RequestParam BigDecimal value) {
        log.debug("credit balance = {} for user id = {}", value, id);
        return accountService.creditRecord(id, value);
    }
}
