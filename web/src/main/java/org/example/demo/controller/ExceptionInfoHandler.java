package org.example.demo.controller;

import org.example.demo.exception.ErrorInfo;
import org.example.demo.exception.NotFoundException;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

import static org.example.demo.exception.ErrorType.*;

@RestControllerAdvice
public class ExceptionInfoHandler {
    private static final Logger log = LoggerFactory.getLogger(ExceptionInfoHandler.class);

    private static final Map<String, String> CONSTRAINS_MAP = Map.of(
            "(email)", "email already existed"
    );

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<ErrorInfo> handleGenericNotFoundException(HttpServletRequest req, NotFoundException e) {
        return getErrorInfoResponseEntity(DATA_NOT_FOUND.getErrorCode(), DATA_NOT_FOUND.getStatus(), req, e);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<ErrorInfo> handleGenericException(HttpServletRequest req, NoHandlerFoundException e) {
        return getErrorInfoResponseEntity(NOT_FOUND_URL.getErrorCode(), NOT_FOUND_URL.getStatus(), req, e);
    }

    @ExceptionHandler(UnsatisfiedServletRequestParameterException.class)
    public ResponseEntity<ErrorInfo> handleGenericException(HttpServletRequest req, UnsatisfiedServletRequestParameterException e) {
        return getErrorInfoResponseEntity(BAD_REQUEST.getErrorCode(), BAD_REQUEST.getStatus(), req, e);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorInfo> handleGenericException(HttpServletRequest req, MethodArgumentTypeMismatchException e) {
        return getErrorInfoResponseEntity(BAD_REQUEST.getErrorCode(), BAD_REQUEST.getStatus(), req, e);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ErrorInfo> handleGenericException(HttpServletRequest req, MissingServletRequestParameterException e) {
        return getErrorInfoResponseEntity(BAD_REQUEST.getErrorCode(), BAD_REQUEST.getStatus(), req, e);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorInfo> handleGenericException(HttpServletRequest req, HttpMessageNotReadableException e) {
        return getErrorInfoResponseEntity(BAD_REQUEST.getErrorCode(), BAD_REQUEST.getStatus(), req, e);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorInfo> handleGenericException(HttpServletRequest req, MethodArgumentNotValidException e) {
        String[] strings = e.getBindingResult().getFieldErrors().stream()
                .map(fieldError -> fieldError.getField() + " " + fieldError.getDefaultMessage())
                .toArray(String[]::new);

        return getErrorInfoResponseEntity(VALIDATION_ERROR.getErrorCode(), VALIDATION_ERROR.getStatus(), req, e, strings);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorInfo> handleGenericException(HttpServletRequest req, ConstraintViolationException e) {
        String message = getMessage(getRootCause(e));
        if (message != null) {
            for (Map.Entry<String, String> entry : CONSTRAINS_MAP.entrySet()) {
                if (message.toLowerCase().contains(entry.getKey())) {
                    return getErrorInfoResponseEntity(UNIQUE_ERROR.getErrorCode(), UNIQUE_ERROR.getStatus(), req, e, entry.getValue());
                }
            }
        }
        return getErrorInfoResponseEntity(DATA_ERROR.getErrorCode(), DATA_ERROR.getStatus(), req, e);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorInfo> handleGenericException(HttpServletRequest req, Exception e) {
        return getErrorInfoResponseEntity(APP_ERROR.getErrorCode(), APP_ERROR.getStatus(), req, e);
    }

    private ResponseEntity<ErrorInfo> getErrorInfoResponseEntity(
            String errorType, HttpStatus httpStatus, HttpServletRequest req, Exception e, String... details
    ) {

        ErrorInfo error = new ErrorInfo(errorType, details.length != 0 ? details : new String[]{getMessage(getRootCause(e))});
        error.setTimestamp(LocalDateTime.now());
        error.setHttpStatus((httpStatus.value()));
        if (Objects.isNull(req.getQueryString())) {
            error.setRequest(req.getRequestURI());
        } else {
            error.setRequest(req.getRequestURI() + "?" + req.getQueryString());
        }
        log.warn("{} at request  {}: {}", errorType, req.getRequestURL(), error.getErrorMsg());
        return new ResponseEntity<>(error, httpStatus);
    }

    private static Throwable getRootCause(Throwable t) {
        Throwable result = t;
        Throwable cause;

        while (null != (cause = result.getCause()) && (result != cause)) {
            result = cause;
        }
        return result;
    }

    private static String getMessage(Throwable e) {
        return e.getLocalizedMessage() != null ? e.getLocalizedMessage() : e.getClass().getName();
    }
}
