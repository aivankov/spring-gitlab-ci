package org.example.demo.to;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.example.demo.model.Gender;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "email")
public class AccountTo implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @Email
    private String email;

    @NotNull
    private LocalDate birthday;

    @Enumerated(EnumType.STRING)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @NotNull
    private Gender gender;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @NotNull
    private BigDecimal balance = BigDecimal.ZERO;
}
