package org.example.demo.service;

import org.example.demo.exception.ErrorMessage;
import org.example.demo.exception.IllegalRequestDataException;
import org.example.demo.exception.NotFoundException;
import org.example.demo.model.Account;
import org.example.demo.repository.datatable.AccountDataTableRepository;
import org.example.demo.repository.jpa.AccountRepository;
import org.example.demo.to.AccountTo;
import org.example.demo.util.AccountUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class AccountService {

    private final AccountRepository repository;
    private final AccountDataTableRepository dataTableRepository;

    public AccountService(AccountRepository repository, AccountDataTableRepository dataTableRepository) {
        this.repository = repository;
        this.dataTableRepository = dataTableRepository;
    }

    public Optional<Account> getWithOptional(Long id) {
        return repository.findById(id);

    }

    public Account get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException(ErrorMessage.NOT_FOUND_ID.getMessage() + id));
    }

    public Page<Account> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    }


    public Account getByEmail(String email) {
        return repository.findByEmail(email)
                .orElseThrow(() -> new NotFoundException(ErrorMessage.NOT_FOUND_EMAIL.getMessage() + email));
    }

    @Transactional
    public void delete(Long id) {
        if (repository.delete(id) != 1) {
            throw new NotFoundException(ErrorMessage.NOT_FOUND_ID.getMessage() + id);
        }
    }

    @Transactional
    public void update(AccountTo accountTo, Long id) {
        if (Objects.isNull(accountTo.getId())) {
            throw new IllegalRequestDataException("id must not be null");
        }
        if (!id.equals(accountTo.getId())) {
            throw new IllegalRequestDataException("must be with id=" + id);
        }
        Account account = AccountUtil.updateFromTo(accountTo);
        conformityRules(account);
        repository.save(account);
    }

    @Transactional
    public Account save(AccountTo accountTo) {
        Account account = AccountUtil.updateFromTo(accountTo);
        if (Objects.nonNull(account.getId())) {
            throw new IllegalRequestDataException("id must be null");
        }
        conformityRules(account);
        return repository.save(account);
    }

    @Transactional
    public BigDecimal debitRecord(Long id, BigDecimal value) {
        Account account = repository.findById(id)
                .orElseThrow(() -> new NotFoundException(ErrorMessage.NOT_FOUND_ID.getMessage() + id));
        account.setBalance(account.getBalance().add(value));
        return account.getBalance();
    }

    @Transactional
    public BigDecimal creditRecord(Long id, BigDecimal value) {
        Account account = repository.findById(id)
                .orElseThrow(() -> new NotFoundException(ErrorMessage.NOT_FOUND_ID.getMessage() + id));
        account.setBalance(account.getBalance().subtract(value));
        return account.getBalance();
    }

    public DataTablesOutput<Account> findAll(DataTablesInput input) {
        return dataTableRepository.findAll(input);
    }

    private void conformityRules(Account account) {
        String email = account.getEmail();
        if (Objects.nonNull(email)) {
            account.setEmail(email.toLowerCase());
        }

        String firstName = account.getFirstName();
        if (Objects.nonNull(firstName) && !firstName.isBlank()) {
            account.setFirstName(firstName.substring(0, 1).toUpperCase() + firstName.substring(1).toLowerCase());
        }

        String lastName = account.getLastName();
        if (Objects.nonNull(lastName) && lastName.isBlank()) {
            account.setLastName(lastName.substring(0, 1).toUpperCase() + lastName.substring(1).toLowerCase());
        }
    }
}
