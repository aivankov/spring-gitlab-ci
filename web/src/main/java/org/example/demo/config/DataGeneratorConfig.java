package org.example.demo.config;

import org.example.demo.repository.jpa.AccountRepository;
import org.example.demo.util.AccountDataGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class DataGeneratorConfig {

    private final AccountRepository accountRepository;

    @Value("${generate.count:0}")
    private int countGenerateData;

    public DataGeneratorConfig(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Bean
    public AccountDataGenerator dataGenerator() {
        return new AccountDataGenerator();
    }

    @PostConstruct
    public void afterPropertiesSet() {
        if(countGenerateData > 0 && accountRepository.findAll().isEmpty()) {
            accountRepository.saveAll(dataGenerator().generateAccountList(countGenerateData));
        }
    }
}
