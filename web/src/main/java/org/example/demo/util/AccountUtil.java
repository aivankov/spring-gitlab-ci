package org.example.demo.util;

import org.example.demo.model.Account;
import org.example.demo.to.AccountTo;

public class AccountUtil {
    private AccountUtil() {
    }

    public static Account updateFromTo(AccountTo accountTo) {
        return Account.builder()
                .id(accountTo.getId())
                .firstName(accountTo.getFirstName())
                .lastName(accountTo.getLastName())
                .email(accountTo.getEmail())
                .birthday(accountTo.getBirthday())
                .gender(accountTo.getGender())
                .balance(accountTo.getBalance())
                .build();
    }
}
