package org.example.demo.repository.datatable;

import org.example.demo.model.Account;
import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

public interface AccountDataTableRepository extends DataTablesRepository<Account, Long> {
}
