package org.example.demo.util;

import org.example.demo.model.Account;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class AccountDataGeneratorTest {
    private final AccountDataGenerator generator = new AccountDataGenerator();

    @Test
    void generateAccount() {
        Account account = generator.generateAccount();
        assertFalse(account.getFirstName().isEmpty());
        assertFalse(account.getLastName().isEmpty());
        assertFalse(account.getEmail().isEmpty());
    }

    @Test
    void generateAccountList() {
        final int count = 10;
        List<Account> accounts = generator.generateAccountList(count);
        assertEquals(count, accounts.size());
        assertFalse(accounts.get(0).getFirstName().isEmpty());
        assertFalse(accounts.get(0).getLastName().isEmpty());
        assertFalse(accounts.get(0).getGender().toString().isEmpty());
    }

    @Test
    void uniqueEmail() {
        final int countAccount = 30;
        final int countCycle = 10;

        for (int i = 0; i < countCycle; i++) {
            List<Account> accountList = generator.generateAccountList(countAccount);
            Set<Account> accountsSet = new HashSet<>(accountList);
            assertEquals(accountList.size(), accountsSet.size());
        }
    }
}
