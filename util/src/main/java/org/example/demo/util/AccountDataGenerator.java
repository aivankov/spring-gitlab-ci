package org.example.demo.util;

import com.devskiller.jfairy.Fairy;
import com.devskiller.jfairy.producer.person.Person;
import org.example.demo.model.Account;
import org.example.demo.model.Gender;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class AccountDataGenerator {

    public Account generateAccount() {
        Fairy fairy = Fairy.create();
        Person person = fairy.person();
        Random random = new Random();

        Account account = new Account();
        account.setFirstName(person.getFirstName());
        account.setLastName(person.getLastName());
        account.setEmail(person.getEmail());
        account.setBirthday(person.getDateOfBirth());
        account.setGender(Gender.valueOf(person.getSex().name()));
        account.setBalance(BigDecimal.valueOf(random.nextInt(200_000)));

        return account;
    }

    public List<Account> generateAccountList(int size) {
        // because sometimes generator gives to not unique email
        int upperCount = 10 + size;
        return Stream.generate(this::generateAccount)
                .limit(upperCount)
                .distinct()
                .limit(size)
                .collect(toList());
    }
}
