package org.example.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "email")
@Builder
@Entity
@Table(name = "accounts")
public class Account {
    @Id
    @SequenceGenerator(name = "global_seq", sequenceName = "global_seq", allocationSize = 1, initialValue = 10)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "global_seq")
    private Long id;

    @Column(name = "first_name", nullable = false)
    @NotBlank
    private String firstName;

    @Column(name = "last_name", nullable = false)
    @NotBlank
    private String lastName;

    @Column(unique = true)
    @Email
    private String email;

    @Column(nullable = false)
    @NotNull
    private LocalDate birthday;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull
    private Gender gender;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @NotNull
    private BigDecimal balance = BigDecimal.ZERO;
}

