package org.example.demo.model;

public enum Gender {
    MALE,
    FEMALE
}
