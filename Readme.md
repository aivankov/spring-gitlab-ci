## A spring boot 2 sample  project !

[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=aivankov_spring-gitlab-ci&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=aivankov_spring-gitlab-ci)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=aivankov_spring-gitlab-ci&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=aivankov_spring-gitlab-ci)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=aivankov_spring-gitlab-ci&metric=security_rating)](https://sonarcloud.io/dashboard?id=aivankov_spring-gitlab-ci)

## Technical Stack

- Java 11
- Maven 3.6+
- Spring boot 2.4.5
- Lombok abstraction
- Flyway
- JPA with H2 for testing
- JPA with Postgres for production
- REST API
- AJAX 
- JavaScript
- Bootstrap 4
- Thymeleaf
- GitLab
- Sonar 

Heroku: [spring-gitlab-ci](https://spring-gitlab-ci.herokuapp.com/)

Rest Docs: [html](./web/docs/rest_api_guide.html), [pdf](./web/docs/rest_api_guide.pdf)

